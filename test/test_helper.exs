ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Crickapp.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Crickapp.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Crickapp.Repo)

